# <img src="logo.png" height=150 alt="Keep a Changelog" />

[![version][version-badge]][CHANGELOG] [![license][license-badge]][LICENSE]
# felsiusJS
A module to simply conversion to 'felsius' a new unit of temperature mentioned in XKCD 1923.

1. Require it. 
```javascript
var felsius = require('felsiusJS');
```
2. Use it.
```javascript
var c = felsius.toFelsius(9, 'c');
var f = felsius.toFelsius(9, 'f');
var f = felsius.toFelsius(9, 'k');
var f = felsius.fromFelsius(9, 'k');
var f = felsius.fromFelsius(9, 'c');
var f = felsius.fromFelsius(9, 'f');
```
The second argument is for the unit. c for celsius, f for fahrenheit and k for kelvin. Can be omitted and will if so defualt to celsius. 

The 'k' option is available in version v0.9.0 and onwards.

[version-badge]: https://img.shields.io/badge/version-0.8.0-blue.svg
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg
[CHANGELOG]: ./CHANGELOG.md
[LICENSE]: ./LICENSE