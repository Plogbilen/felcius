
var celsiusToFelsius = temp => {
  if(temp === 0) {
    return 16;
  } else {
    return ((temp * 7) / 5) + 16;
  }
};

var felsiusToCelsius = temp => {
  return ((temp - 16) * 5) / 7;
};

var fahrenheitToFelsius = temp => {
  if(temp === 0) {
    return -9;
  } else {
    return ((temp * 7) - 80) / 9;
  }
};

var felsiusToFahrenheit = temp => {
  return ((temp * 9) + 80) / 7;
};

var toFelsius = (temp, from='c') => {
  switch(from) {
  case 'k':
    return celsiusToFelsius(temp - 273);
  case 'f':
    return fahrenheitToFelsius(temp);
  case 'c':
  default:
    return celsiusToFelsius(temp);  
  }
};

var fromFelsius = (temp, to='c') => {
  switch(to) {
  case 'k':
    return felsiusToCelsius(temp) + 273;
  case 'f':
    return felsiusToFahrenheit(temp);
  case 'c':
  default:
    return felsiusToCelsius(temp);  
  }
};

module.exports = {
  toFelsius,
  fromFelsius
};