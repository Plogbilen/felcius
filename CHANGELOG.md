# <img src="logo.png" height=150 alt="Keep a Changelog" />

[![version][version-badge]][CHANGELOG] [![license][license-badge]][LICENSE]

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.9.1] - 2017-12-10
### Changed
- Removed most of the functions from the API. The only ones available are: 
```javascript
var x = toFelsius(temp, unit);
var y = fromFelsius(temp, unit);
```
## [0.9.0] - 2017-12-09
### Added
- Support for kelvin, in the fromFelsius and toFelsius functions. Use the 'k' as option as such: 
```javascript
var felsius = require('felsiusjs')
var k = felsius.fromFelsius(11, 'k');
```

## [0.8.0] - 2017-12-08
### Added
- Added new functions to the API. See README.md

## [0.7.3] - 2017-12-06
- Updated npm keywords
- No new functionality

## [0.7.2] - 2017-12-06
- No new functionality

## [0.7.1] - 2017-12-02
- No new functionality

## [0.6.0] - 2017-12-01
- This was supposed to be the preious release.

## [0.0.6] - 2017-12-01
### Added
- README
- LICENSE
- Logo

No added functionality.

## [0.5.0] (First release) - 2017-12-01
### Added
- Core functionality. 

[0.8.0]: https://gitlab.com/Plogbilen/felcius/commit/d072865d6edc25f4fdb93c0786aba8f9bc366b14
[0.7.3]: https://gitlab.com/Plogbilen/felcius/commit/75e99a3ef028f67dbf5a36d73cbb31ff66704d44
[0.7.2]: https://gitlab.com/Plogbilen/felcius/commit/c558c5b015f687fc020868685b4304ae96d6a4db
[0.7.1]: https://gitlab.com/Plogbilen/felcius/commit/6e19e8ce5a761cbf32b5ff4fda9b87894a2c6507
[0.7.0]: https://gitlab.com/Plogbilen/felcius/commit/d1907aa35f2ef0a5ac95dd6a39df9f608d4242ae
[0.6.0]: https://gitlab.com/Plogbilen/felcius/commit/28b3be91b6669c6a23baa00f2e7ee31403a36697
[0.0.6]: https://gitlab.com/Plogbilen/felcius/commit/be366d3ca65f93c47f1071acd00ad73012c4eae9
[0.5.0]: https://gitlab.com/Plogbilen/felcius/commit/7627317c3571268f0a52ef77f26080a8d72c0429

[version-badge]: https://img.shields.io/badge/version-0.8.0-blue.svg
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg
[CHANGELOG]: ./CHANGELOG.md
[LICENSE]: ./LICENSE