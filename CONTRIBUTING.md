# <img src="logo.png" height=150 alt="Keep a Changelog" />

[![version][version-badge]][CHANGELOG] [![license][license-badge]][LICENSE]


## Development
### Dependencies

- NodeJS 8

### Installation
```bash
git clone <url>
cd felsiusjs
npm install
npm start
```

### Testing
```bash
npm test
```

[CHANGELOG]: ./CHANGELOG.md
[LICENSE]: ./LICENSE
[version-badge]: https://img.shields.io/badge/version-1.8.0-blue.svg
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg