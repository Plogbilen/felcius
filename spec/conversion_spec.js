var app = require('../app.js');

describe('Conversion functions', () => {
  it('Celsius to felsius', done => {
    expect(app.toFelsius(0, 'c')).toBe(16);
    done();      
  });
  
  it('Fahrenheit to felsius', done => {
    expect(app.toFelsius(0, 'f')).toBe(-9);  
    done();    
  });

  it('Felsius to celsius', done => {
    expect(app.fromFelsius(16, 'c')).toBe(0);
    done();      
  });

  it('Felsius to fahrenheit', done => {
    expect(app.fromFelsius(-9, 'f')).toBe(-0.14285714285714285);  
    done();    
  });

  it('Felsius to kelvin', done => {
    expect(app.fromFelsius(16, 'k')).toBe(273);
    done();    
  });

  it('Kelvin to felsius', done => {
    expect(app.toFelsius(273, 'k')).toBe(16);
    done();    
  });
});